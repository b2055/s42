// Retrive an element from the webpage
const txtFirstName = document.querySelector('#txt-first-name')
const txtLastName = document.querySelector('#txt-last-name')
const spanFullName = document.querySelector('#span-full-name')
// "document" refers to the whole webpage and "querySelector" is used to select a specific object (HTML element) from the document.

/* 
    Alternatively, we can use getElement functions to retrieve the elements
        document.getElementById('txt-first-name')
        document.getElementByClassName('txt-inputs')
        document.getElementByTagNAme('input')
*/

let firstName = ""
let lastName = ""
let fullName = ""

//performs an action when an event triggers
txtFirstName.addEventListener('keyup', (event) => {
    firstName = txtFirstName.value
    fullNameDisplay()
})

txtLastName.addEventListener('keyup', (event) => {
    lastName = txtLastName.value
    fullNameDisplay()
})

function fullNameDisplay() {
    fullName = firstName + " " + lastName
    spanFullName.innerHTML = fullName
}

/* let text = "testing"
let length = text.length
let test = text.length -1
let reverse = ""

for(let i = 0; i < length; i++) {
    reverse = reverse + text[test]  
    

    test--
}

console.log(reverse) */